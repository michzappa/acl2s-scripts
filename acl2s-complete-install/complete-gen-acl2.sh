#!/usr/bin/env bash

# This script will create a new ACL2 executable and clean all books.
# 
# Follow the instructions for cert.pl from the ACL2 documentation
# topic cert.pl. In particular, see the preliminaries subtopic
# (BUILD____PRELIMINARIES).
#
# Make sure you set the environment variable ACL2_SYSTEM_BOOKS as per
# the cert.pl instructions and make sure that you configured your
# $PATH so that running acl2 will invoke ACL2.
#
# Next, set the environment variable ACL2S_SCRIPTS to the top-level
# directory of this repository.
#
# Next, set the environment variable ACL2_LISP to the lisp you use to build
# ACL2 (SBCL is what I use)

if [[ -z "${ACL2_SYSTEM_BOOKS}" ]]; then
    echo "Error: the environement variable ACL2_SYSTEM_BOOKS must be defined!"
    echo "Set ACL2_SYSTEM_BOOKS as per the cert.pl instructions"
    exit 1
fi

if [[ -z "${ACL2_LISP}" ]]; then
    echo "Error: the environement variable ACL2_LISP must be defined!"
    echo "Set ACL2_LISP to the lisp you use to build ACL2 (SBCL is a good choice)."
    exit 1
fi

# Clean existing books
cd "$ACL2_DIR"
make clean-all
make clean-books 

# Rebuild ACL2
make LISP="$ACL2_LISP"
