#!/usr/bin/env bash

# This script will create an executable file, acl2s, in the current
# directory by building ACL2s from scratch by cloning sbcl,
# acl2, acl2s-scripts repositories and then building sbcl, acl2 and
# acl2s.
#
# In order for this script to work, you should have gnu make, perl,
# etc installed and you should install sbcl using a package management
# system and sbcl should be accessible from your PATH.

export BASE_DIR="$PWD"

# We are downloading the latest version of sbcl. We use the installed
# sbcl to build the new sbcl, so you still need to install a version
# of sbcl.
# On Linux machines, the following command should install sbcl.
# apt-get -y install sbcl

git clone -n git@github.com:sbcl/sbcl.git
cd sbcl
git checkout 1322b2fad824629e038231722d21b8761bd125b5

cd "$BASE_DIR"
git clone -n git@github.com:acl2/acl2.git
cd acl2
git checkout 3aaddfede9853813eaf30179bddbd24fc2c204c1

cd "$BASE_DIR"
git clone git@gitlab.com:acl2s/external-tool-support/scripts.git

cd "$BASE_DIR"
mkdir local
mkdir local/bin
mkdir local/lib

export SBCL_DIR="$BASE_DIR"/sbcl
export SBCL_INSTALL="$BASE_DIR"/local
export ACL2_DIR="$BASE_DIR"/acl2
export ACL2_SYSTEM_BOOKS="$ACL2_DIR"/books
export ACL2S_SCRIPTS="$BASE_DIR"/scripts/acl2s-complete-install

cd "$ACL2S_SCRIPTS"
./complete-install-sbcl.sh

export ACL2_LISP="$SBCL_INSTALL"/bin/sbcl
export SBCL_HOME="$SBCL_INSTALL"/lib/sbcl

cd "$ACL2S_SCRIPTS"
./complete-gen-acl2.sh

export ACL2="$ACL2_DIR"/saved_acl2
export PATH="$PATH":"$ACL2_SYSTEM_BOOKS"/build

cd "$BASE_DIR"
"$ACL2S_SCRIPTS"/complete-gen-acl2s.sh

