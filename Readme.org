* ACL2s
  
The ACL2 Sedan theorem prover (ACL2s) is an extension of ACL2
that provides 

- a powerful termination analysis engine,
- a data definition framework for defining arbitrary types,
- a contract-based function definition framework,
- a seamlessly integrated bug-finding framework
- a property-based design and development framework, and 
- a modern integrated development environment, using Eclipse.

* Quick setup guide 

- Install SBCL using a package manager.
- Clone the [[https://github.com/acl2][ACL2 repo]] somewhere on your computer.
- Set the following variables, say in your .bashrc, as: (replacing the placeholders with the appropriate values for your system)
    #+BEGIN_SRC 
    export ACL2_SYSTEM_BOOKS={path to the acl2 repo}/books 
    export ACL2S_SCRIPTS={path to this repo)
    export ACL2_LISP=sbcl
    export ACL2S_NUM_JOBS={number of your choice}
    #+END_SRC
- Run the script "clean-gen-acl2.sh" in the scripts directory of this
  repository. It will create the acl2 executable, saved_acl2 in your
  acl2 repo.
- Add a symlink named acl2 to the saved_acl2 file generated above. Do
  this in a directory that is in your $PATH, so that you can run acl2
  in any subdirectory by typing "acl2". I use ~/bin for such links. In
  ~/bin (or the directory of your choosing), type the following, where
  you use the actual directory for your acl2 repo.
    #+BEGIN_SRC 
    ln -s {path to the acl2 repo}/saved_acl2 acl2
    #+END_SRC
- Run the script "gen-acl2s.sh". It will create the acl2s executable
  and the acl2.core in the directory from which the script is invoked.
- Add a symlink named acl2s to the acl2s file generated above. Do this
  in a directory that is in your $PATH, so that you can run acl2s in
  any subdirectory by typing "acl2s". I use ~/bin for such links. In
  ~/bin (or the directory of your choosing), type the following, where
  you use the actual directory for your acl2s scripts repo.
    #+BEGIN_SRC 
    ln -s {path to scripts repo}/acl2s acl2s
    #+END_SRC
- Note you cannot run the scripts starting with "rebuild-sbcl", nor is
  there any reason to do that, since you do not have an SBCL
  repository.

* Building ACL2s using the SBCL repository

To build ACL2s, use the following instructions.

 - Perform all the steps in the quick setup guide.
 - Clone the SBCL repository. To build SBCL, you need a
   working LISP compiler, so the first step is still needed.
 - Ensure that you have the dependencies for SBCL installed (in
   particular, a C compiler). If you're using Ubuntu, the
   `build-essential` package should contain everything you need. The
   first step should take care of this.
 - Set the following envirornment variable
    #+BEGIN_SRC 
    export SBCL_DIR={path to the SBCL repo}
    #+END_SRC
 - Run the script "rebuild-sbcl-acl2-acl2s.sh" in the scripts
   directory of this repository. It will pull, rebuild sbcl, acl2 and
   acl2s. It will create an acl2s executable in the directory from
   which the script is invoked.
 - Assuming the SBCL build process succeeded, if you used an older
   version of SBCL to bootstrap SBCL's installation then you can either
   uninstall that older version of SBCL, or update the ACL2_LISP
   environment variable to point to the new version of SBCL that you
   just built. 
 - For who want to see updates to ACL2s right away and for those who
   are modifying the ACL2s books in the ACL2 repository, please use
   the testing-acl2s branch. Master will be more stable, but expect it
   to be about a day behing the testing-acl2s branch.
 - Read the doc topic on [[https://www.cs.utexas.edu/~ragerdl/acl2-manual/index.html?topic=ACL2____0._02Preliminaries][cert.pl]] from the ACL2 webpage for more
   information. 
  
To build a local copy of the manual, which is very useful for working
offline, run the script "gen-manual.sh" in the scripts directory of
this repository. This takes a while, which is why we do not build it
as part of the ACL2s build process.

* ACL2s with emacs

You can now run the generated acl2s executable in an emacs shell
buffer.  You can use the keybindings in the .lisp.el file in this
repository. Alternatively, you can find keybindings in
emacs/emacs-acl2.el in the acl2 git repo.

* Rebuilding ACL2s

When ACL2s and/or ACL2 libraries are subsequently updated, just run
the "gen-acl2s.sh" script again to generate an updated acl2s executable.

When ACL2 is subsequently updated, you can rebuild ACL2 using the
"clean-gen-acl2.sh" script. To rebuild ACL2 and ACL2s, 
use "clean-gen-acl2-acl2s.sh" script.

It is also a good idea to update your underlying LISP compiler every
once in a while. See the "rebuild-sbcl.sh" script.

